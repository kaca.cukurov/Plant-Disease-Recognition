import cv2
from sklearn.cluster import KMeans


def getLeafPoints(path):
    image = cv2.imread(path)
    height, width, ch = image.shape

    imageSeparated = cv2.imread(path)

    image = image.reshape((image.shape[0] * image.shape[1], 3))

    # cluster the pixel intensities
    clt = KMeans(n_clusters=2)
    clt.fit(image)

    bcgLabel = 0

    if clt.labels_[2] != 0:
        bcgLabel = 1

    bcgPoints = []
    leafPoints = []

    i = 0
    for label in clt.labels_:
        if label == bcgLabel:
            bcgPoints.append([i % width, i // width])
        else:
            leafPoints.append([i % width, i // width])
        i += 1

    for p in bcgPoints:
        imageSeparated[p[1], p[0]] = [0, 0, 0]

    # cv2.imshow("result", imageSeparated)
    # cv2.waitKey(0)
    return leafPoints


def getLeafPixels(points, image):
    imagePixels = []
    for p in points:
        imagePixels.append(image[p[1], p[0]])
    return imagePixels
