from background_removal import getLeafPoints
from utils import findAngles


def loadImagesForTest():
    grapesHealthy, cherriesHealthy, grapesSick, cherriesSick = [], [], [], []

    for i in range(1, 6):
        grapesHealthy.append('imagesGrapes/healthy/p' + str(i) + '.jpg')
        cherriesHealthy.append('imagesCherry/healthy/p' + str(i) + '.jpg')
        grapesSick.append('imagesGrapes/powderyMildew/p' + str(i) + '.jpg')

    for i in range(1, 5):
        cherriesSick.append('imagesCherry/powderyMildew/p' + str(i) + '.jpg')

    return grapesHealthy, cherriesHealthy, grapesSick, cherriesSick


def loadImagesForNetwork():
    listGrapes, listCherries, listCherriesTest, listGrapesTest = [], [], [], []
    listGrapesData, listGrapesDataTest, listCherriesData, listCherriesDataTest = [], [], [], []

    for i in range(1, 6):
        listGrapes.append('imagesGrapes/healthy/p' + str(i) + '.jpg')
        listCherries.append('imagesCherry/healthy/p' + str(i) + '.jpg')
        listCherriesTest.append('imagesCherry/test/p' + str(i) + '.jpg')
        listGrapesTest.append('imagesGrapes/test/p' + str(i) + '.jpg')

    for grapeLeaf in listGrapes:
        loaded = getDataFromImage(grapeLeaf)
        loaded.append(1)  # GRAPE = 1
        listGrapesData.append(loaded)

    for cherryLeaf in listCherries:
        loaded = getDataFromImage(cherryLeaf)
        loaded.append(0)  # CHERRY = 0
        listCherriesData.append(loaded)

    for grapeLeafTest in listGrapesTest:
        loaded = getDataFromImage(grapeLeafTest)
        loaded.append(1)  # GRAPE = 1
        listGrapesDataTest.append(loaded)

    for cherryLeafTest in listCherriesTest:
        loaded = getDataFromImage(cherryLeafTest)
        loaded.append(0)  # CHERRY = 0
        listCherriesDataTest.append(loaded)

    return [listGrapesData, listCherriesData, listGrapesDataTest, listCherriesDataTest]


def getDataFromImage(path):
    leafPoints = getLeafPoints(path)

    left = leafPoints[0]
    right = leafPoints[0]
    top = leafPoints[0]
    bottom = leafPoints[0]

    for p in leafPoints:
        if p[0] < left[0]:
            left = [p[0], p[1]]
        if p[0] > right[0]:
            right = [p[0], p[1]]
        if p[1] > bottom[1]:
            bottom = [p[0], p[1]]
        if p[1] < top[1]:
            top = [p[0], p[1]]

    leafWidth = right[0] - left[0]
    leafHeight = bottom[1] - top[1]

    leafWidthScaled = leafWidth / leafHeight
    leafHeighScaled = leafHeight / leafWidth

    angles = findAngles([left, top, right, bottom])

    print('\n\nHeight : ' + str(leafHeighScaled))
    print('Width : ' + str(leafWidthScaled))
    print('Angles : ' + str(angles))
    print()

    return [[leafWidthScaled, 0, 0, 0], [leafHeighScaled, 0, 0, 0], angles]
