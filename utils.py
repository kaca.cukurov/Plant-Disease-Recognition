import math


def findAngles(points):
    angles = []

    i = 0
    length = len(points)

    while i < length:  # calculating angles between points of leaf contour

        idxBefore = (i + length - 1) % length
        idx = i
        idxAfter = (i + 1) % length

        p01 = math.sqrt(
            (points[idx][0] - points[idxBefore][0]) ** 2 + (points[idx][1] - points[idxBefore][1]) ** 2)
        p02 = math.sqrt(
            (points[idx][0] - points[idxAfter][0]) ** 2 + (points[idx][1] - points[idxAfter][1]) ** 2)
        p12 = math.sqrt((points[idxBefore][0] - points[idxAfter][0]) ** 2 + (
                points[idxBefore][1] - points[idxAfter][1]) ** 2)

        dividor = (2 * p01 * p02)
        if dividor == 0:
            dividor = 1
        angle = math.acos((p01 ** 2 + p02 ** 2 - p12 ** 2) / dividor)
        angles.append(angle)

        i += 1

    return angles
