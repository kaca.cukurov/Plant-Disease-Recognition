import cv2
from sklearn.cluster import KMeans

import background_removal


def checkLeaf(path):
    sickClusterMinSize = 50
    whiteValueMin = 500
    clusterNo = 10

    image = cv2.imread(path)

    leafPoints = background_removal.getLeafPoints(path)
    leafPixels = background_removal.getLeafPixels(leafPoints, image)

    # cluster the pixel intensities
    clt = KMeans(n_clusters=clusterNo)
    clt.fit(leafPixels)

    clusters = {}
    small_clusters = []
    for item in clt.labels_:
        if item in clusters:
            clusters[item] = clusters[item] + 1
        else:
            clusters[item] = 0

    for item in clusters:
        if clusters[item] < sickClusterMinSize:
            small_clusters.append(item)

    # if size of cluster is too small we ignore it
    n = 0
    sick = False
    for c in clt.cluster_centers_:
        if n in small_clusters:
            continue
        n += 1
        if c[0] + c[1] + c[2] > whiteValueMin:
            sick = True
            print("The plant has powdery mildew.")
            print(c)

    if not sick:
        print("The plant is healthy!")
