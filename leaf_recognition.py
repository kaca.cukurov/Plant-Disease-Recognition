import numpy as np
from keras.layers import Dense
from keras.layers import Flatten
from keras.models import Sequential

model = Sequential()
epochs = 150


def train_neural_network(cherries, grapes, cherriesTest, grapesTest):
    X = []
    Y = []

    paramNo = len(cherries[0]) - 1
    batchSize = len(cherries[0][0])

    for cherry in cherries:
        X.append(cherry[0:paramNo])
        Y.append(cherry[paramNo])

    for grape in grapes:
        X.append(grape[0:paramNo])
        Y.append(grape[paramNo])

    xTest, yTest = [], []

    for cherry in cherriesTest:
        xTest.append(cherry[0:paramNo])
        yTest.append(cherry[paramNo])

    for grape in grapesTest:
        xTest.append(grape[0:paramNo])
        yTest.append(grape[paramNo])

    model.add(Flatten(input_shape=(paramNo, batchSize)))
    model.add(Dense(units=paramNo, activation='sigmoid'))
    model.add(Dense(units=1, activation='sigmoid'))

    model.compile(loss='binary_crossentropy',
                  optimizer='sgd',
                  metrics=['accuracy'])

    # batch size determines number of X elements which we want to process
    model.fit(np.array(X), Y, epochs=epochs, batch_size=batchSize)

    # evaluate the model
    scores = model.evaluate(np.array(xTest), yTest)
    print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1] * 100))


def getLeafType(data):
    prediction = model.predict_classes(np.array([data]))

    if prediction == 0:
        print('The plant is cherry.')
    else:
        print('The plant is grape.')
