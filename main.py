import data_loader
import disease_recognition
import leaf_recognition


def test(leaves):
    for leaf in leaves:
        testImageData = data_loader.getDataFromImage(leaf)

        # Get the type of the test leaf
        leaf_recognition.getLeafType(testImageData)

        # Check if the test leaf is healthy
        disease_recognition.checkLeaf(leaf)


grapesHealthy, cherriesHealthy, grapesSick, cherriesSick = data_loader.loadImagesForTest()

# Load images for neural network training and get the data from them
data = data_loader.loadImagesForNetwork()

# Train the neural network
leaf_recognition.train_neural_network(data[0], data[1], data[2], data[3])

test(grapesHealthy)
test(cherriesHealthy)
test(grapesSick)
test(cherriesSick)
